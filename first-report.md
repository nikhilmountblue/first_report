# Improving The Performance and Scaling using DataBases.
* In the following report, there are information regarding **NOSQL**.
* How **Performance and Scaling** can be improved by using NOSQL.
* Picked Top few databases which are very frequently used in the Industry.
* Bellow report explicitly speaks of the pros and cons of individuals databases.  

Referred Links:

[ Introduction on NOSQL ](https://www.trustradius.com/nosql-databases)

[ Importance of NOSQL ](https://www.couchbase.com/resources/why-nosql#:~:text=NoSQL%20database%20technology%20is%20a,management%20demands%20of%20modern%20businesses.
) - This link gives a very accurate information  NOSQL.



## NOSQL

### Introduction on NOSQL

NOSQL database provides a mechanism for storing and retrieving data very efficiently than the traditional way of handling the data. This traditional way of storing the data was very efficient when there was no internet, mobile, etc.., so the traditional method played a vital role in maintaining the database. The expansion of the global network leads to the requirement of a more efficient and high scalable database. Where the traditional database failed to meet the demands efficiently.

### Importance of NOSQL

* In Relational Database Management System the data model is fixed and schema is pre-defined. NOSQL is schema-less and doesn't pre-define how data must be molded. This provides more flexibility for developers.

    ```
                CReating a table using SQL.
    
                CREATE TABLE Persons (
                PersonID int,
                LastName varchar(255),
                FirstName varchar(255),
                Address varchar(255),
                City varchar(255)
                );
    ```
    As shown above in a Relational Database Management System we need prefix the data type that we are about to store.

    ```                 
                Creating a table using MongoDB(NOSQl).

                >use database;
                >database.createCollection("Person");

    ```

    As shown above in NOSQL we just need to create a collection and we need to push the data into this collection and store it in our database.

* In Relational Database Management System data should be shredded into small pieces of information and stored in different tables and made relations between these tables but in NOSQL there is no requirement of shredding the information rather the data is stored as an object in the form of JSON in the database.

![](fig1.png)

The above image shows how the data are shredded and saved into tables in a Relational Database Management System. 


![](fig2.png)

The above image shows how the data is stored in NOSQL as an object(JSON format). 


* The way in which NOSQL stores data gives developers a very good platform to extract data in the form of an object and much faster then Relational Database Management System.

* Due to Global networking, there is a rapid increase in users and data exchange globally and every industry need to maintain the data efficiently and cost-efficiently.

![](loadgraph.png)

The above graph shows how proportionally load increases as the users increase.

![](cost.png)

The above graph shows how cost varies with increasing users with different databases.


## NOSQL DataBases

### 1. MONGODB

* MongoDB has high levels of reading and writing traffic.
* MongoDB has the capability of collaborating a large number of developers on a single project.
* Scale your data repository to a massive size.
* Evolve the type of deployment as the business changes.
* Examples where MongoDB is being used.

    * Used in storing Adhar details.
    * Shutterfly uses MongoDB to share pictures.
    * eBay uses MongoDB to save it users data.


### 2.CASSANDRA

* Cassandra can handle structured, semi-structured and unstructured data, this allows the database to be more flexible with data storage.
* Cassandra uses multiple data centers which allows for easy distribution of data at all times and places.
* Cassandra support the ACID property.
* Cassandra is a column-oriented database.
* Examples where Cassandra is being used.

    * Used in the Reddit industry for storing data.
    * Cassandra is being used in OpenX for Storing and replicating 130 nodes.
<<<<<<< HEAD
    * Cassandra is being used in Netflix for a Back-end database.
    * Cassandra is being used in Apple for 100000 nodes.

### 3.DYNAMODB

* DynamoDB is designed and developed by Amazon.com.It runs on the Cross-Platform operating system.
* DynamoDB doesn't need to have set up process on our machine.
* DynamoDB uses AWS console and navigate through wizards which enables us to create the database.
* DynamoDB has the most sophisticated security when compared to other NOSQL databases.
* DynamoDB support only following languages like 

    * JAVA
    * JAVASCRIPT
    * Swift
    * Node.js
    * .Net
    * PHP
    * Python
* Examples where DynamoDB is being used.

    * BMW
    * Netflix
    * Samsung Electronics
    * tinder

### 4.NEO4J 

* J in Neo4j stands for Java and this software requires Java development kit (JDK) to perform operation data or run.
* Cypher is one of the interactive query languages that is used in Neo4j.
* Neo4j uses graphs to represent the database.
* Neo4j comes with the web browser utility tool to provide data visualization and other features such as speed.
* Neo4j supports importing data from external sources.
* Examples where Neo4j is being used.

    * Retailers like Walmart.
    * Aircraft Manufacturer like Airbus rely.
    * Insurance Companies like Optum HealthCare.
    * Banks like JP Morgan Chase.

### 5.COUCHBASE 

* Couchbase uses Http protocols to interact with different types of objects and they are called  **BUCKETS**.
* Couchbase uses views concepts to display data from database which very similar to Relational the Database Management System.
* Couchbase is an offline database that can be installed on a mobile phone and which the very uses full while designing an android application and io applications.
* Couchbase have a inbuilt dashboard which is used as an information management tool.
* Examples where CouchBase is being used.

    * Linkedin
    * Paypal
    * ebay
    * Doddle
    * Cassandra is being used in Netflix for a Back-end database.
    * Cassandra is being used in Apple for 100000 nodes.